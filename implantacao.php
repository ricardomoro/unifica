<?php
/*
Template Name: Páginas Implantação
*/

get_header(); ?>

<?php breadcrumb(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post();?>

<div class="container" id="conteudo">
	<div class="row"> 
        <div class="col-md-3">
            <aside class="conteudo">
    			<?php wp_nav_menu( array('menu' => 'menu-implantacao' )); ?>
            </aside>
        </div>
        <div class="col-md-9" >
            <section class="conteudo">
                <article>
                    <?php the_content(); ?>
                </article>
            </section>
        </div>
    </div>
</div>

<?php endwhile; endif; ?>

<?php get_footer(); ?>
