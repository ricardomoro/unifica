<?php get_header(); ?>

<?php breadcrumb(); ?>

<div class="container" id="conteudo">
    <div class="row">
        <div class="col-md-8">
            <section class="conteudo">
            <?php while (have_posts()) : the_post(); ?>
                <article>
                    <h1><?php the_title();?></h1>
                    <small>
                        Escrito por <b><?php the_author(); ?></b>. <?php the_time('j'); ?> de <?php the_time('F'); ?> de <?php the_time('Y'); ?>, às <?php the_time(); ?>
                    </small>

                    <!-- Insere a imagem destaque na notícia, caso tenha. -->
                    <?php if ( has_post_thumbnail()): ?>
                        <div style="float:left;margin: 0 1.75em 2.5em 0;">
                            <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
                            <a class="fancybox" rel="gallery1" href="<?php echo $url; ?>">
                                <?php the_post_thumbnail( array(320,225) ); ?>
                            </a>
                        </div>
                    <?php endif; ?>
                    <!-- FIM - Insere a imagem destaque na notícia, caso tenha. -->
                
                    <?php the_content(); ?>
                </article>
            <?php endwhile; ?>
            </section>
        </div>

        <div class="col-md-4">
            <aside id="lista_noticias" class="conteudo box">
                <h2><a href="<?php echo site_url(); ?>/perguntas/">Perguntas Frequentes</a></h2>
                <ul>           
                <?php $permalink = get_permalink(); ?> 

                <?php 
                    $args = array(
                        'post__not_in'  => array($post->ID),//exclui notícia atual
                        'category_name' => 'perguntas',     //categoria Perguntas
                        'posts_per_page' => 10,             //máximo de listagem
                        );
                    query_posts( $args ); 
                ?>

                <?php while (have_posts()) : the_post(); ?>
                    <li>
                    <?php if ( has_post_thumbnail()): ?>
                        <div class="miniatura">
                            <a href="<?php the_permalink(); ?>" 
                               title="<?php printf( esc_attr__( ' %s', 'thirdstyle' ), the_title_attribute( 'echo=0' ) ); ?>">
                               <?php the_post_thumbnail( array(100,100) ); ?>
                            </a>
                        </div>

                        <a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( ' %s', 'thirdstyle' ), the_title_attribute( 'echo=0' ) ); ?>">
                            <?php the_title(); ?>
                        </a>
                    <?php else: ?>
                        <div style=" padding:10px;">
                            <a href="<?php the_permalink(); ?>" 
                               title="<?php printf( esc_attr__( ' %s', 'thirdstyle' ), the_title_attribute( 'echo=0' ) ); ?>">
                               <?php the_title(); ?>
                            </a>                      
                        </div>
                    <?php endif; ?>
                    </li>
                    <span class="linha"></span>
                <?php endwhile; ?>
                <?php wp_reset_query(); ?>    
                </ul>
                
                <div class="linha">
                    <a href="<?php echo site_url(); ?>/perguntas/">Todas as perguntas</a>
                </div>
            </aside>
        </div>
    </div>
</div>

<?php get_footer(); ?>
