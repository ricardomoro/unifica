<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Título -->
    <title><?php wp_title( '|', true, 'right' ); ?></title>

    <!-- Favicon -->
    <link rel="shortcut icon"        type="image/x-icon" href="<?php echo get_template_directory_uri(); ?>/img/favicon.ico"/>

    <!-- CSS -->
    <link rel="stylesheet"           type="text/css"     href="<?php echo get_template_directory_uri(); ?>/style.css" />    
    <link rel="alternate stylesheet" type="text/css"     href="<?php echo get_template_directory_uri(); ?>/style-contraste.css" title="contraste" />    
    <link rel="stylesheet"           type="text/css"     href="<?php echo get_template_directory_uri(); ?>/js/fancybox/jquery.fancybox.css?v=2.1.4"  media="screen" />
    
    <!-- JS -->
    <!--[if lt IE 9]>
        <script src="<?php echo get_template_directory_uri(); ?>/js/html5shiv.js"></script>
        <script src="<?php echo get_template_directory_uri(); ?>/js/respond.min.js"></script>
    <![endif]-->
    
    <!--[if lt IE 7]>
        <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/bootstrap-ie7.css" />
    <![endif]-->
    
    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery-2.1.0.min.js"              type="text/javascript"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"                 type="text/javascript"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/fancybox/jquery.fancybox.pack.js" type="text/javascript"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/styleswitcher.js"                 type="text/javascript"></script>

    <?php wp_head(); ?>
</head>
<body>
    <!-- Barra do Governo -->
    <div id="barra-brasil">
        <a href="http://brasil.gov.br">Portal do Governo Brasileiro</a>
    </div>

    <!-- Barra de Acessibilidade -->
    <div class="container hidden-xs">
        <div class="row">
            <div class="col-xs-12">
                <ul id="atalhos">
                    <li><a href="#inicio-conteudo"            accesskey="1">Conteúdo <span class="badge">1</span></a></li>
                    <li><a href="#inicio-menu"                accesskey="2">Menu <span class="badge">2</span></a></li>
                    <li><a href="#search-acessibilidade"      accesskey="3">Pesquisar <span class="badge">3</span></a></li>
                    <li><a href="#contraste-ligar"            accesskey="4" id="contraste-ligar"><span class="glyphicon glyphicon-adjust"></span> Contraste <span class="badge">4</span></a></li>
                    <li><a href="#contraste-desligar"         accesskey="4" id="contraste-desligar"><span class="glyphicon glyphicon-adjust"></span> Contraste <span class="badge">4</span></a></li>
                </ul>
            </div>
        </div>
    </div>
                    
    <header>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1>
                        <a href="<?php echo home_url(); ?>" id="logo">
                            <span class="sr-only">P&aacute;gina Inicial Unifica IFRS</span>
                        </a>
                    </h1>
                </div>
            </div>
        </div>
    </header>

    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <a href="#inicio-menu" id="inicio-menu" class="sr-only">In&iacute;cio do menu</a>
                <nav class="navbar navbar-default" role="navigation">
                    <div class="navbar-header">
                      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu-principal">
                        <span class="sr-only">Alternar Navega&ccedil;&atilde;o</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                      </button>
                    </div>

                    <div class="collapse navbar-collapse" id="menu-principal">
                        <?php wp_nav_menu( array('container' => false, 'menu_class' => 'nav navbar-nav') ); ?>
                        <form method="get" action="<?php bloginfo('url'); ?>/" class="navbar-right col-md-3" role="search" id="form-busca">
                            <div class="input-group">
                                <input type="text" name="s" class="form-control" placeholder="Buscar..." id="search-acessibilidade" />
                                <div class="input-group-btn">
                                    <button class="btn btn-success" value="Pesquisar">
                                        <span class="glyphicon glyphicon-search"></span>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </nav>
                <a href="#fim-menu" id="fim-menu" class="sr-only">Fim do menu</a>
            </div>
        </div>
    </div>

    <a href="#inicio-conteudo" id="inicio-conteudo" class="sr-only">In&iacute;cio do conte&uacute;do</a>
