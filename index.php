<?php get_header(); ?>

<div class="container" id="conteudo">
    <?php easingsliderlite(); ?>

    <div class="row">
        <div class="col-md-6">
            <section class="conteudo">
                <div class="row">
                    <div class="col-xs-12">
                        <h2><a href="<?php bloginfo('url'); ?>/noticias/">Not&iacute;cias</a></h2>
                    </div>
                </div>
                <?php query_posts('category_name=noticias&showposts=3'); ?>
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                    <div class="row">
                        <div class="col-xs-12">
                            <article>
                                <h3><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h3>
                                <?php if ( has_post_thumbnail()): ?>
                                    <div class="col-md-7" style="padding-left:0;">
                                        <?php the_post_thumbnail( 'medium', array('class' => 'img-no-tamanho-da-div') ); ?>
                                    </div>
                                <?php endif; ?>

                                <?php echo the_excerpt(); ?>

                                <small><?php the_time('d F Y') ?></small>
                                
                                <a href="<?php the_permalink() ?>" rel="bookmark" class="btn btn-success pull-right">
                                    Leia mais<span class="sr-only"> sobre &ldquo;<?php the_title(); ?>&rdquo;</span>
                                </a>
                                <hr />
                            </article>
                        </div>
                    </div>
                <?php endwhile; endif; ?>
            </section>
        </div>
    
        <aside class="col-md-6">
            <div class="row">
                <div class="col-xs-12">
                    <section class="conteudo">
                        <div class="embed-responsive embed-responsive-4by3">
                            <iframe class="embed-responsive-item" width="100%" height="315" src="//www.youtube.com/embed/XSykdGsg0Wo" allowfullscreen></iframe>
                        </div>
                        <hr/>
                        <div class="embed-responsive embed-responsive-4by3">
                            <iframe class="embed-responsive-item" width="100%" height="315" src="//www.youtube.com/embed/5slOZP4tm_k" allowfullscreen></iframe>
                        </div>
                    </section>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <section class="conteudo">
                        <h2><a href="<?php bloginfo('url'); ?>/perguntas/">Perguntas Frequentes</a></h2>
                        <?php query_posts('category_name=perguntas&showposts=5'); ?>
                        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                            <h3><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
                            <?php echo the_excerpt(); ?>
                            <hr class="linha"></hr>
                        <?php endwhile; endif; ?>
                    </section>
                </div>
            </div>
        </aside>
    </div>
</div>

<?php get_footer(); ?>
