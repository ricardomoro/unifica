    <a href="#fim-conteudo" id="fim-conteudo" class="sr-only">Fim do conte&uacute;do</a>
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <a href="http://www.ifrs.edu.br/" title="Site do IFRS">
                        <img src="<?php echo get_template_directory_uri();?>/img/logo-ifrs-branco.png" alt="Site do IFRS" />
                    </a>
                </div>
                <div class="col-md-4 center">
                    <!-- Título do Projeto -->
                    <p class="titulo"><strong>Projeto Unifica IFRS</strong></p>
                    <!-- Email do Projeto -->
                    <p><a href="mailto:unifica@ifrs.edu.br">unifica@ifrs.edu.br</a></p>
                </div>
                <div class="col-md-4 col-lg-3 col-lg-offset-1 center">
                    <!-- Wordpress -->
                    <p>
                        <a href="http://www.wordpress.org/">Desenvolvido com Wordpress</a>
                    </p>
                    <!-- GPLv3 -->
                    <p>
                        <a href="https://bitbucket.org/ricardomoro/unifica/">C&oacute;digo-fonte deste tema sob a licen&ccedil;a GPLv3</a>
                    </p>
                    <!-- Creative Commons 4.0 -->
                    <p>
                        <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/">
                            <img alt="Licença Creative Commons" style="border-width:0" src="http://i.creativecommons.org/l/by-nc-nd/4.0/80x15.png" />
                        </a>
                        <br />
                        M&iacute;dia licenciada com uma Licen&ccedil;a <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/">Creative Commons Atribui&ccedil;&atilde;o-N&atilde;oComercial-SemDeriva&ccedil;&otilde;es 4.0 Internacional</a>
                    </p>
                </div>
            </div>
        </div>
    </footer>

    <?php wp_footer(); ?>

    <script type="text/javascript">
        $(document).ready(function() {
            $(".fancybox").fancybox();
        });
    </script>
    <script src="http://barra.brasil.gov.br/barra.js?cor=verde"                                   type="text/javascript" async="async"></script>
</body>
</html>
