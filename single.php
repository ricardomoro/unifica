<?php

foreach(get_the_category() as $category) {
	 
	switch($category->slug){

		case 'noticias':
			include (TEMPLATEPATH.'/single-noticias.php');
			break;

		case 'perguntas':
			include (TEMPLATEPATH.'/single-perguntas.php');
			break;
	
		default :
			include (TEMPLATEPATH.'/single-noticias.php');	
			break;	

	}
}

?>
