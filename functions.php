<?php

//função migalhas ou breadcrumb
require_once ('inc/breadcrumb.php');

//lightbox das imagens
require_once ('inc/lightbox.php');

//cria o títudo do site dinamicamente
require_once ('inc/titulo_site.php');

//Paginação
require_once ('inc/pagination.php');

//Habilita imagens destaques
add_theme_support( 'post-thumbnails' );

//Registra o menu principal
register_nav_menus(
	array(
		'menu-principal' => 'Menu Principal',
		'menu-implantacao' => 'Menu Implantação',
	));

?>