<?php
/*
Template Name: Páginas Geral
*/

get_header(); ?>

<?php breadcrumb(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post();?>

<div class="container" id="conteudo">
	<div class="row"> 
        <div class="col-xs-12">
            <section class="conteudo">
                <article>
                    <?php the_content(); ?>
                </article>
            </section>
        </div>
	</div>
</div>

<?php endwhile; endif; ?>

<?php get_footer(); ?>
